#!/usr/bin/nodejs
'use strict';

const express = require('express');

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();
app.get('/', (req, res) => {
  res.send('<h1>This is my app on Heroku, Hello There :D</h1>');
});

app.get('/*', (req, res) => {
  res.send('<h1>Not available.</h1>');
});

app.listen(PORT, function() {
  console.log('Express server listening on port ' + PORT);
});